package models;

import java.util.Date;

public class Visit {
     private Customer customer;
     private Date date;
     private double serviceExpense;
     private double productExpense;


     public Visit(Customer customer, Date date) {
          this.customer = customer;
          this.date = date;
     }


     public Customer getCustomer() {
          return customer;
     }


     public double getServiceExpense() {
          return serviceExpense;
     }


     public void setServiceExpense(double serviceExpense) {
          this.serviceExpense = serviceExpense;
     }


     public double getProductExpense() {
          return productExpense;
     }


     public void setProductExpense(double productExpense) {
          this.productExpense = productExpense;
     }

     public double getTotalExpense(){
          return serviceExpense + productExpense;
     }

     @Override
     public String toString() {
          return "Visit[" + this.getCustomer() + ", date = " + this.date + ", serviceExpense = " + this.serviceExpense + ", productExpense = " + this.productExpense + "]";
     }
     
}
