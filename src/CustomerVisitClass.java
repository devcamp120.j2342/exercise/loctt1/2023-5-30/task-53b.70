import java.util.Date;

import models.Customer;
import models.Visit;

public class CustomerVisitClass{
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Truong Tan Loc");
        customer1.setMember(true);
        customer1.setMemberType("Gold");
        System.out.println("Customer 1:");
        System.out.println(customer1);
        System.out.println("-----------------------------------------------");

        Customer customer2 = new Customer("Vuong Thi My Cam");
        customer2.setMember(true);
        customer2.setMemberType("Silver");
        System.out.println("Customer 2:");
        System.out.println(customer2);
        System.out.println("-----------------------------------------------");

        Visit visit1 = new Visit(customer1, new Date(27));
        visit1.setServiceExpense(20);
        visit1.setProductExpense(30.5);
        System.out.println("Visit 1:");
        System.out.println(visit1);
        System.out.println("---------------------------------------------------");

        Visit visit2 = new Visit(customer2, new Date(30));
        visit1.setServiceExpense(40);
        visit1.setProductExpense(50.5);
        System.out.println("Visit 2:");
        System.out.println(visit2);
        System.out.println("---------------------------------------------------");

    }
}
